# Der make Befehl
In diesem Dokument steht, wie du dir auf deinem PC die Pdf Datein selber erstellen kannst und was du noch so alles mit dem make Befehl machen kannst. Der make Befehl gefolgt von einem Kommando wird immer im Hauptordner des Repos ausgeführt.

## Make all
Mit dem nachfolgenden Befehl werden alle möglichen Version an Pdf erstellt und in /conf/out abgelegt. Dieser Buildprozess kann, je nachdem wie umfangreich ein Repo ist, sehr lange dauern. Wenn du im vorhinein bereits weißt welche Version du möchtest, kannst du auch explizit nur bestimmte Versionen generieren. Siehe dazu Selektiv erstellen.

```
$ make all
```

## Selektiv erstellen
Der selektive make Befehl besteht aus 7 Optionen, welche in der folgenden Rheinfolge, durch einen Bindestrich getrennt, aufgerufen werden.

```
make {opn,cld}-{exm,lab,skp,ue}-{bsp,mdl,mpc,thr,txt}-{ang,lsg,hyb}-{cmp,pge}-{one,two}-{max,avg}
```

### Erste Option
Welche Berechtigungsstufe soll gewählt werden?

```
opn		Open		Es werden nur Inhalte aus dem open Bereich kompiliert
cld		Closed		Es werden open und closed Inhalte kompiliert, Berechtigung vorausgesetzt
```

### Zweite Option
Aus welchem Bereich soll gewählt werden?

```
exm		Prüfung		Nur Inhalte von Prüfungen
lab		Labor		Nur Inhalte von Laboren
skp		Skriptum	Nur Inhalte von Skripten
ue		Übung		Nur Inhalte von Übungen
```

### Dritte Option
Welche Untergruppe soll gewählt werden?

```
bsp		Beispiel	Rechenbeispiele
mdl		Mündlich	Inhalte einer Mündlichen Prüfung
mpc		Mehrfachauswahl	Inhalte von Multiple Choice Fragen
thr		Theorie		Theoriefragen
txt		Text		Erklärender Text
```

### Vierte Option
Wie sollen die Aufgaben ausgedruckt werden?

```
ang		Angabe		Ausschließlich die Angabe zu einer Frage
lsg		Lösung		Ausschließlich die Lösung zu einer Frage
hyb		Hybrid		Angabe und Lösung zu einer Frage
```

### Fünfte Option
Wie sollen die Aufgaben voneinander getrennt sein?

```
cmp		Kompakt		Fragen und Lösungen befinden sich unmittelbar nacheinander
pge		Seite		Jede Frage beginnt auf einer neuen Seite. z.B. um unterhalb eine eigene Lösung zu erarbeiten
```

### Sechste Option
Wie wird das Dokument ausgedruckt? Wird ein einseitiges Dokument auf einem Drucker doppelseitig ausgedruckt, wird der Text beim Lochen oder Binden, beschädigt oder verdeckt.

```
one		Einseitig	Das PDF ist für einseitigen Ausdruck optimiert
two		Zweiseitig	Das PDF ist für zweiseitigen Ausdruck optimiert
```

### Siebte Option
Wie soll das Seitenlayout ausgenützt werden?

```
max		Maximal		Das Seitenlayout wird maximal ausgenützt
avg		Normal		Das Seitenlayout wird laut Latex Definition ausgenützt
```

## Ordnerstruktur Anlegen
Wenn du Inhalte hinzufügen möchtest, kannst du folgenden Befehl aufrufen. Dieser erstellt wir ein leeres Ordnerverzeichnis mit allen möglichen Kombinationen. Damit kannst du ganz bequem deine Datein ablegen.

```
$ make file
```

## Aufräumen
Genau das Gegenteil von make file kann mit diesem Befehl erreicht werden. Es werden alle Ordner entfernt, welche keinen Inhalt haben. Ebenfalls werden Builddatein entfernt.

```
$ make clean
```

## Update
Diese Option wird nur von den Maintainern aufgerufen um die README, CONTRIBUTING, gitignore und andere Datein in den Repos zu aktualisieren. Du benötigst sie nicht.
