%% Aufgabe 1

%Definition der auszuwählenden Funktion
func = @calcf_styblinksi_tang;

clear x f df ddf;
%Simulationswerte
x=zeros(2,101);
x(1,:)=linspace(-5,5,101);
x(1,:)=linspace(-5,5,101);

%Auswertung an einer Gerade
a=[-5;-5];
b=1/sqrt(2)*[1;1];
t=linspace(0,10*sqrt(2),101);
x_g=b.*t + a;

f=zeros(1,size(x_g,2));
df=zeros(2,size(x_g,2));
for i=1:size(x_g,2)
[f(i),df(:,i),~]=func(x_g(:,i));

end

plot(x_g,f)

d=df.*b;

%Schrittweite
h=1e-9;

for i=1:size(x_g,2)
    [f_n(1,i),~,~]=func(x_g(:,i)+h*[1;0]);
    [f_n(2,i),~,~]=func(x_g(:,i)-h*[1;0]);
    [f_n(3,i),~,~]=func(x_g(:,i)+h*[0;1]);
    [f_n(4,i),~,~]=func(x_g(:,i)-h*[0;1]);    
end

df_n(1,:)=(f_n(1,:)-f_n(2,:))/(2*h);
df_n(2,:)=(f_n(3,:)-f_n(4,:))/(2*h);

e_df=df-df_n;
e=abs(e_df);
plot(e)
%% Aufgabe 2




%% Aufgabe 3




%% Aufgabe 4