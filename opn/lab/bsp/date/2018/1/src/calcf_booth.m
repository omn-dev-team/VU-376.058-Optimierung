function [f,df,ddf] = calcf_booth(x)
%Booth Funktion

f=(x(1,:)+2.*x(2,:)'-7).^2 +(x(2,:)'+2.*x(1,:)-5).^2;

%Darstellen der Funktion
%meshc(x(1,:),x(2,:),f)
%df=gradient(f);
%clear m n;
%syms m n;
%df=gradient((m+2*n-7)^2+(2*m+n-5)^2,[m, n]);
%ddf=hessian((m+2*n-7)^2+(2*m+n-5)^2,[m, n]);
df(:,:,1)=10.*x(1,:)+8.*x(2,:)'-34;
df(:,:,2)=8.*x(1,:)+10.*x(2,:)'-38;
ddf(:,:,1,1)=ones(size(x,2))*10;
ddf(:,:,1,2)=ones(size(x,2))*8;
ddf(:,:,2,1)=ones(size(x,2))*8;
ddf(:,:,2,2)=ones(size(x,2))*10;
end

