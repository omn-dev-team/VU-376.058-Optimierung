function [f,df,ddf] = calcf_styblinksi_tang(x)
%Styblinksi Funktion

f=0;
f=0.5.*((x(1,:).^4-16.*x(1,:).^2+5.*x(1,:))+(x(2,:)'.^4-16.*x(2,:)'.^2+5.*x(2,:)'));

%Darstellen der Funktion
%meshc(x(1,:),x(2,:),f)
%df=gradient(f);
%clear m n;
%syms m n;

%df=gradient(0.5*((m^4-16*m^2+5*m)+(n^4-16*n^2+5*n)));
%ddf=hessian(0.5*((m^4-16*m^2+5*m)+(n^4-16*n^2+5*n)),[m,n]);
%meshc(x(1,:),x(2,:),df)

df(:,:,1)=ones(size(x,2),1)*(2.*x(1,:).^3-16.*x(1,:) + 5/2);
df(:,:,2)=ones(size(x,2),1)*(2.*x(2,:).^3 - 16.*x(2,:) +5/2);
ddf(:,:,1,1)=ones(size(x,2),1)*(6.*x(1,:).^2-16);
ddf(:,:,1,2)=0;
ddf(:,:,2,2)=(6.*(x(2,:).^2)' -16)*ones(1,size(x,2),1);
ddf(:,:,2,1)=0;
end

